## This app is built in gradle. It uses google juice dependency injection, it is written in Java 8

## Test application
./testApp.sh

## Run application, the main method simulate a persion who is over 70 years age for a collection of services. 
./runApp.sh