package uk.co.songt;

import com.google.inject.Inject;
import org.javatuples.Pair;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.songt.imp.DiscountRuleImp;
import uk.co.songt.juice.module.ApplicationModule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JukitoRunner.class)
public class DiscountRuleImpTest {

    public static class TestModule extends JukitoModule {
        @Override
        protected void configureTest() {
            install(new ApplicationModule());
        }
    }

    @Inject
    private DiscountRule discountRule;

    List<Product> products;

    @Before
    public void setUp() {
        products = new ArrayList<Product>();
    }

    @After
    public void tearDown() {
        products = new ArrayList<Product>();
    }

    @Test
    public void getTotalPriceTest() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        Pair<PatientType, List<Product>> patientTypeListPair = new Pair<>(PatientType.SUPER_SENIOR,products);
        BigDecimal total = discountRule.getTotalPrice(products);
        assertThat(total.floatValue(), is(138.00f));
    }

    @Test
    public void getDiscountPriceTest() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        BigDecimal discountPrice = discountRule.getDiscountPrice(PatientType.SUPER_SENIOR,discountRule.getTotalPrice(products));
        assertThat(discountPrice.floatValue(), is(124.20f));
    }

    @Test
    public void getDiscountRuleTest() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        Pair<PatientType, List<Product>> patientTypeListPair = new Pair<>(PatientType.SUPER_SENIOR,products);
        BigDecimal discountPrice = discountRule.getDiscountRule().apply(patientTypeListPair);
        assertThat(discountPrice.floatValue(), is(124.20f));
    }

    @Test
    public void getVipDiscountRuleTestWithNonVIP() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.ECG);
        Pair<PatientType, List<Product>> patientTypeListPair = new Pair<>(PatientType.SUPER_SENIOR,products);
        BigDecimal discountPrice = discountRule.getVipDiscountRule().apply(patientTypeListPair);
        assertThat(discountPrice.floatValue(), is(234.36f));
    }

    @Test
    public void getVipDiscountRuleTestWithVIP() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        products.add(Product.BLOODTEST);
        Pair<PatientType, List<Product>> patientTypeListPair = new Pair<>(PatientType.VIP,products);
        BigDecimal discountPrice = discountRule.getVipDiscountRule().apply(patientTypeListPair);
        assertThat(discountPrice.floatValue(), is(23.40f));
    }

    @Test
    public void testGetRuleReturnNull() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        DiscountRuleImp discountRuleImp = mock(DiscountRuleImp.class);
        final Map<PatientType, Rule> DISCOUNT_RULES = new HashMap<PatientType, Rule>();
        when(discountRuleImp.initializeDiscountRules()).thenReturn(DISCOUNT_RULES);
        assert discountRuleImp.getRule(PatientType.CHILDREN.SENIOR)==null;
    }
}
