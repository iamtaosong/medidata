package uk.co.songt;



import com.google.inject.Inject;
import org.javatuples.Pair;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoRule;
import uk.co.songt.imp.CheckoutImp;
import uk.co.songt.imp.DiscountRuleImp;
import uk.co.songt.juice.module.ApplicationModule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(JukitoRunner.class)
public class CheckoutImpTest  {

    public static class TestModule extends JukitoModule {

        @Override
        protected void configureTest() {
            install(new ApplicationModule());
        }
    }

    @Inject
    Checkout checkout;
    List<Product> products;
    @Inject
    DiscountRule discountRule;
    @Before
    public void setUp() {
        products = new ArrayList<Product>();
    }

    @After
    public void tearDown() {
        products = new ArrayList<Product>();
    }

    @Test
    public void calculatePriceTest() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        discountRule.initializeDiscountRules();
        BigDecimal total = checkout.calculatePrice(PatientType.SUPER_SENIOR,products);
        assertThat(total.floatValue(), is(13.80f));
    }

    @Test
    public void calculatePriceTestWithVIP() {
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        products.add(Product.BLOODTEST);
        discountRule.initializeDiscountRules();
        BigDecimal total = checkout.calculatePrice(PatientType.VIP,products);
        assertThat(total.floatValue(),  is(192.6F));
    }

}
