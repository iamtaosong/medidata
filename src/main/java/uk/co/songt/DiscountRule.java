package uk.co.songt;


import org.javatuples.Pair;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface DiscountRule {

    public Rule getRule(PatientType patientType);

    public Map<PatientType, Rule> initializeDiscountRules();

    public Function<Pair< PatientType , List<Product>>, BigDecimal> getDiscountRule();

    public Function<Pair< PatientType , List<Product>>, BigDecimal> getVipDiscountRule();

    public BigDecimal getTotalPrice(List<Product> products);

    public BigDecimal getDiscountPrice(PatientType patientType,BigDecimal totalPrice);

}
