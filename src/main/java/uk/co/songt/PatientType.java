package uk.co.songt;


import java.math.BigDecimal;

public enum  PatientType {
    SUPER_SENIOR("over 70", new BigDecimal(0.90)),
    SENIOR("between 65 and 70", new BigDecimal(0.60)),
    CHILDREN("under 5", new BigDecimal(0.40)),
    VIP("with MediHealth Health insurance", new BigDecimal(0.150));

    private String description;
    private BigDecimal discount;

    PatientType(String description, BigDecimal discount){
        this.description=description;
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getDiscount() {
        return discount;
    }
}
