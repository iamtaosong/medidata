package uk.co.songt;


import org.javatuples.Pair;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

public class Rule {

    public final Function<Pair< PatientType , List<Product>>,BigDecimal> rule;

    public Rule(Function<Pair< PatientType , List<Product>>, BigDecimal> rule){
        this.rule = rule;
    }

}
