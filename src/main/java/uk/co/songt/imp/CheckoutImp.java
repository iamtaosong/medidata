package uk.co.songt.imp;


import com.google.inject.Inject;
import org.javatuples.Pair;
import uk.co.songt.Checkout;
import uk.co.songt.DiscountRule;
import uk.co.songt.PatientType;
import uk.co.songt.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class CheckoutImp implements Checkout {

    @Inject
    DiscountRule discountRule;

    public BigDecimal calculatePrice(PatientType patientType, List<Product> products) {
        BigDecimal totoalPrice = discountRule.getTotalPrice(products);
        if (discountRule.getRule(patientType) != null) {
            BigDecimal discount = discountRule.getRule(patientType).rule.apply(new Pair<>(patientType, products));
            return totoalPrice.subtract(discount);
        }
        return totoalPrice;
    }

}
