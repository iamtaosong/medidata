package uk.co.songt.imp;

import org.javatuples.Pair;
import uk.co.songt.DiscountRule;
import uk.co.songt.PatientType;
import uk.co.songt.Product;
import uk.co.songt.Rule;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DiscountRuleImp implements DiscountRule {

    static final Map<PatientType, Rule> DISCOUNT_RULES = new HashMap<PatientType, Rule>();
    static int ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;
    static int DECIMALS = 2;

    public Rule getRule(PatientType patientType) {
        return DISCOUNT_RULES.get(patientType);
    }

    public  Map<PatientType, Rule> initializeDiscountRules() {
        DISCOUNT_RULES.put(PatientType.SUPER_SENIOR, new Rule(getDiscountRule()));
        DISCOUNT_RULES.put(PatientType.SENIOR, new Rule(getDiscountRule()));
        DISCOUNT_RULES.put(PatientType.CHILDREN,new Rule(getDiscountRule()));
        DISCOUNT_RULES.put(PatientType.VIP,new Rule(getVipDiscountRule()));
        return DISCOUNT_RULES;
    }

    public BigDecimal getTotalPrice(List<Product> products){

        return rounded(products.stream().map(product -> product.getPrice()).reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public BigDecimal getDiscountPrice(PatientType patientType,BigDecimal totalPrice){
        return rounded(totalPrice.multiply(patientType.getDiscount()));
    }

    public Function<Pair< PatientType , List<Product>>, BigDecimal> getDiscountRule() {

        return new Function<Pair< PatientType , List<Product>>, BigDecimal>() {
            @Override
            public BigDecimal apply(Pair< PatientType , List<Product>> patientProducts) {
                List<Product> products = patientProducts.getValue1();
                PatientType patientType = patientProducts.getValue0();
                BigDecimal totalPrice = getTotalPrice(products);
                return getDiscountPrice(patientType,totalPrice);
            }
        };
    }

    private BigDecimal rounded(BigDecimal aNumber){
        return aNumber.setScale(DECIMALS, ROUNDING_MODE);
    }

    public Function<Pair<PatientType , List<Product>>, BigDecimal> getVipDiscountRule(){
        return new Function<Pair< PatientType , List<Product>>, BigDecimal>() {
            @Override
            public BigDecimal apply(Pair< PatientType , List<Product>> patientTypeListPair) {

                List<Product> products =  patientTypeListPair.getValue1();
                PatientType patientType =  patientTypeListPair.getValue0();
                List<Product> bloodTests = products.stream().filter(product -> product==Product.BLOODTEST).collect(Collectors.toList());
                if(!bloodTests.isEmpty() && patientType.equals(PatientType.VIP) ){
                    BigDecimal bloodTestPrice =  getTotalPrice(bloodTests);
                    return   getDiscountPrice(patientType, bloodTestPrice);
                }
                else{
                    return getDiscountPrice(patientType,  getTotalPrice(products) );
                }
            }
        };
    }
}