package uk.co.songt.juice.module;


import com.google.inject.Binder;
import com.google.inject.Module;
import uk.co.songt.Checkout;
import uk.co.songt.DiscountRule;
import uk.co.songt.imp.CheckoutImp;
import uk.co.songt.imp.DiscountRuleImp;

public class ServiceModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(Checkout.class).to(CheckoutImp.class).asEagerSingleton();
        binder.bind(DiscountRule.class).to(DiscountRuleImp.class).asEagerSingleton();

    }


}
