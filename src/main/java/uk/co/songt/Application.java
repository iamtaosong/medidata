package uk.co.songt;

import com.google.inject.Guice;
import com.google.inject.Injector;
import uk.co.songt.juice.module.ApplicationModule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args){
        Injector injector = Guice.createInjector(new ApplicationModule());
        Checkout checkout = injector.getInstance(Checkout.class);
        DiscountRule discountRule = injector.getInstance(DiscountRule.class);
        discountRule.initializeDiscountRules();
        List<Product> products= new ArrayList<Product>();
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        products.add(Product.DIAGNOSIS);
        products.add(Product.BLOODTEST);
        BigDecimal total =  checkout.calculatePrice(PatientType.SUPER_SENIOR,products);
        System.out.print("bill total :  " + total);
    }
}
