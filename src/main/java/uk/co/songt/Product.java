package uk.co.songt;

import java.math.BigDecimal;

public enum Product {

    DIAGNOSIS("Diagnosis", new BigDecimal(60.00) ),
    XRAY("X-Ray", new BigDecimal(150.00)),
    BLOODTEST("Blood Test",  new BigDecimal(78.00)),
    ECG("ECG", new BigDecimal(200.40)),
    VACCINE_SERVICE("Vaccine service", new BigDecimal(27.50)),
    VACCINE("Vaccine", new BigDecimal(15.0));

    private String name;
    private BigDecimal price;

    private Product(String name, BigDecimal price){
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

}
