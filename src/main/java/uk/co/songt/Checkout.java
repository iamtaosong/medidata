package uk.co.songt;


import java.math.BigDecimal;
import java.util.List;

public interface Checkout {

    public BigDecimal calculatePrice(PatientType patientType, List<Product> products);
}
